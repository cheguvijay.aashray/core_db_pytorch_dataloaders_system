#!/usr/bin/env python
import os

os.system("clear")
from vit_pytorch.deepvit import DeepViT
from vit_pytorch import vit as simple_vit
from vit_pytorch import vit_for_small_dataset as vit_sd
import sys

cuda_device = "cuda:1"

from coredldev.dataset import CoReDataset
from coredldev.finders.distance_scaling.h5 import h5Finder
from coredldev.sources.distance_scaling.h5 import h5Source
from coredldev.utilites.pipeline import pipeline
from coredldev.preprocessing.ligo_noise.inject_noise import noise_injection
from coredldev.preprocessing.raw_postmerger.detector_angle_mixing import (
    detector_angle_mixing,
)
from coredldev.preprocessing.ligo_noise.file_noise import generate_noise
from coredldev.preprocessing.raw_postmerger.distance_scale import distance_scale
from coredldev.preprocessing.raw_postmerger.wavelet_transform import wavelet_transform
from coredldev.preprocessing.to_tensor import to_tensor_clean
from coredldev.dataloaders import train_validation_test_dataloaders

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.utils.data import DataLoader
import time
import numpy as np
import wandb
import datetime
from collections import OrderedDict
from torch import autograd
import matplotlib.pyplot as plt
from torchmetrics import (
    Accuracy,
    MetricCollection,
    AUROC,
    Precision,
    Recall,
    F1Score,
    FBetaScore,
    ROC,
)
import pandas as pd
from torch.multiprocessing import set_start_method, freeze_support

set_start_method("spawn", force=True)
datapoints, eosmap, remaining = h5Finder(sync=True).get_datapoints()
source = h5Source(eos_to_index_map=eosmap, sync=True)
complete_dataset = CoReDataset(
    source,
    datapoints,
    pipeline(
        [
            detector_angle_mixing(),
            distance_scale(),
            wavelet_transform(),
            generate_noise(),
            to_tensor_clean(),
        ]
    ),
)

acc = Accuracy(task="multiclass", num_classes=19)
combined = MetricCollection(
    [
        acc,
        AUROC(task="multiclass", num_classes=19),
        Precision(task="multiclass", num_classes=19),
        Recall(task="multiclass", num_classes=19),
        F1Score(task="multiclass", num_classes=19),
        FBetaScore(task="multiclass", num_classes=19, beta=0.5),
    ]
)


def get_df_from_rdict(rdict):
    return pd.DataFrame(pd.Series(rdict).map(lambda x: x.cpu().item())).T


def init_model():
    # return simple_vit.ViT(image_size=400,
    #                patch_size=20,
    #                num_classes=19,
    #                dim=int(1024/2),
    #                depth=2,
    #                heads=8,
    #                mlp_dim=int(2048/2),
    #                channels=1).to(cuda_device)
    # return vit_sd.ViT(image_size=400,
    #                patch_size=20,
    #                num_classes=19,
    #                dim=1024,
    #                depth=4,
    #                heads=16,
    #                mlp_dim=int(2048/2),
    #                dropout = 0.1,
    #                emb_dropout = 0,
    #                channels=1).to(cuda_device)
    return (
        nn.DataParallel(
            DeepViT(
                image_size=400,
                patch_size=20,
                num_classes=19,
                dim=1024,
                depth=6,
                heads=20,
                mlp_dim=int(2048 / 2),
                dropout=0.1,
                emb_dropout=0.1,
                channels=1,
            ),
            device_ids=[1, 2, 3, 4, 5, 6, 7],
        )
        .cuda()
        .to("cuda:1")
    )


# In[ ]:


dumstring = " "


def calc_metrics(model: torch.nn.Module, dl: DataLoader):
    model.eval()
    raw_output = []
    parameters = []
    with torch.no_grad():
        for batch, (sg, params) in enumerate(dl):
            stime = time.time()
            sg = sg.to(cuda_device).float()
            sgsh = sg.shape
            sg = sg.view(sgsh[0], 1, sgsh[1], sgsh[2]).to(cuda_device)

            params = params[:, 0].to(cuda_device).to(torch.long)
            raw_output.append(model(sg).detach().cpu())
            parameters.append(params.cpu())
            print(
                f"{batch+1} / {len(dl)} {time.time()-stime} { dumstring  * 200 }",
                end="\r",
            )
    model.train()
    output = torch.vstack(raw_output)
    parameters = torch.concat(parameters, dim=0)
    return combined(output.cpu(), parameters.cpu())


model = init_model().to("cuda:1")
startlr = 3e-5
optimizer = optim.AdamW(params=model.parameters(), lr=startlr)
optimizer1 = optim.NAdam(params=model.parameters(), lr=startlr)
step_scheduler = optim.lr_scheduler.MultiStepLR(
    optimizer, milestones=[1, 2, 3, 4], gamma=0.5
)
# at the end of 600 epochs, the learning rate is 0.000,002,62
scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=1, gamma=0.8)
scheduler_pl = torch.optim.lr_scheduler.ReduceLROnPlateau(
    optimizer=optimizer, mode="max", factor=0.7, patience=35, verbose=True
)
lossfn = nn.CrossEntropyLoss()


def to_seconds(s):
    return f"{s//3600}H:{(s%3600)//60}M:{round(s%60,3)}S"


def ismult(n, div):
    return bool(1 >> (n % div))


def save_model(best_model, config, m1, m2, m1name="acc", m2name="auc"):
    name = f"./for_ligo_cluster/saved_models/VIT_Classifier_LIGONOISE_best_model_state_dict_ViT_regressor_for{config.run_name}_stime_{config.start_time.replace(':', '-')}__{m1name}_{m1}__{m2name}_{m2}.pt"
    try:
        torch.save(
            best_model,
            name,
        )
        print("\nSAVING MODEL")
        wandb.save(name)
    except:
        wandb.alert(
            "ruh oh",
            level="warning",
            title="OUT OF MEMORY",
        )


def train_eval_model(config, train_dl, test_dl, adam=True, nadam=True):
    ldl = len(train_dl)
    pre_acc, max_acc, max_auc = 0, 0, 0
    results = pd.DataFrame()
    best_model = OrderedDict()
    for epoch in range(1, config.epochs + 1):
        print("preeval finished")
        etime = time.time()
        for batch, (sg, params) in enumerate(train_dl):
            stime = time.time()
            sgsh = sg.shape
            sg = sg.to(cuda_device).to(torch.float).view(sgsh[0], 1, sgsh[1], sgsh[2])
            params = params[:, 0].to(cuda_device).to(torch.long)
            optimizer.zero_grad()
            outputs = model(sg)
            loss = lossfn(outputs, params)
            loss.backward()
            optimizer.step() if adam else None
            optimizer1.step() if nadam else None
            #
            torch.cuda.empty_cache()
            #
            curr_acc = acc(outputs.to("cpu"), params.to("cpu"))
            wandb.log(
                {
                    "loss": loss.item(),
                    "batch_accuracy": curr_acc,
                    "lr": scheduler.get_last_lr()[0],
                    "epoch": epoch,
                }
            )

            print(
                f"{epoch:5}/{config.epochs:5} // {batch+1:5}/{ldl:5} | Loss: {loss.item():2.4},batch_accuracy:{curr_acc:3.4}, lr:{scheduler.get_last_lr()[0]:1.5}, Time per Batch: {time.time()-stime:.3} seconds, Accumulated Time {to_seconds(round(time.time()-etime,3))}    ",
                end="\r",
                flush=True,
            )

            if (batch - 1) % 30000 == 0:
                print("\nRunning Eval - This will take a WHILE", end="\r")
                epoch_results = calc_metrics(model, test_dl)
                results = pd.concat([results, get_df_from_rdict(epoch_results)])
                max_acc = max(results["MulticlassAccuracy"])
                max_auc = max(results["MulticlassAUROC"])
                #
                if pre_acc < max_acc:
                    best_model = model.state_dict()
                    try:
                        save_model(
                            best_model,
                            config,
                            list(epoch_results.values())[0],
                            list(epoch_results.values())[1],
                        )
                    except:
                        wandb.alert(
                            "whoops :shrug: :skull:",
                            level="warning",
                            title="OUT OF MEMORY",
                        )

                wandb.log(
                    {"epoch": epoch, "lr": scheduler.get_last_lr()[0]}
                    | epoch_results
                    | {
                        "MaximumMulticlassAccuracy": max_acc,
                        "MaximumMulticlassAUROC": max_auc,
                    }
                    | {"EpochTime": time.time() - etime}
                )
        #
        scheduler.step()
        step_scheduler.step()
        scheduler_pl.step(max_acc)
        #
        epoch_results = calc_metrics(model, test_dl)
        results = pd.concat([results, get_df_from_rdict(epoch_results)])
        max_acc = max(results["MulticlassAccuracy"])
        max_auc = max(results["MulticlassAUROC"])

        wandb.log(
            {"epoch": epoch, "lr": scheduler.get_last_lr()[0]}
            | epoch_results
            | {"MaximumMulticlassAccuracy": max_acc, "MaximumMulticlassAUROC": max_auc}
            | {"EpochTime": time.time() - etime}
        )

    epoch_results = calc_metrics(model, test_dl)
    results = pd.concat([results, get_df_from_rdict(epoch_results)])
    return max_acc, max_auc


if __name__ == "__main__":
    # freeze_support()
    # uncomment for training
    results = []
    trials = 1
    for i in range(trials):
        wandb.init(
            project="ldas-test-test",
        )
        config = wandb.config
        config.run_name = wandb.run._run_id
        config = wandb.config
        config.epochs = 5
        config.inx = 400
        config.iny = 400
        config.lr = startlr
        config.trial = i + 1
        config.total_trials = trials
        config.best_model = OrderedDict()
        config.start_time = datetime.datetime.now().isoformat()
        config.savename = f"best_model_state_dict_at_for{config.run_name}_stime_{config.start_time.replace(':', '-')}__acc_max_acc__auc_auc.pt"
        train_dl, valid_dl, test_dl = train_validation_test_dataloaders(
            complete_dataset,
            train_split=0.7,
            test_split=0.15,
            validation_split=0.15,
            train_batch_size=16 * 3,
            validation_batch_size=16 * 5,
            test_batch_size=16 * 5,
            shuffle_dataset=True,
        )
        max_acc, max_auc = train_eval_model(
            wandb.config, train_dl, valid_dl, nadam=True
        )
        results.append(calc_metrics(model, test_dl))  # type: ignore
        if i != (trials - 1):
            model = init_model()

    save_model(config.best_model, config, max_acc, max_auc)

    evaldl = test_dl
    model.eval()
    raw_output = []
    parameters = []
    eoscomp = []

    with torch.no_grad():
        for batch, (sg, params) in enumerate(evaldl):
            sg = sg.to(cuda_device).to(torch.float)
            sgsh = sg.shape
            sg = sg.view(sgsh[0], 1, sgsh[1], sgsh[2])

            eoscomp.append(params[:, 0].to(cuda_device).to(torch.long))

            raw_output.append(model(sg).detach().cpu())
            parameters.append(params.cpu())
            print(batch, "finished")
    output = torch.vstack(raw_output)
    parameters = torch.hstack(parameters)
    roc = ROC(task="multiclass", num_classes=19)
    fpr, tpr, thresholds = roc(output, eoscomp)

    wandb.log({"fpr": fpr, "tpr": tpr, "thresholds": thresholds})
    torch.save([fpr, tpr, thresholds], "./roc.pt")
    wandb.save("./roc.pt")

    output = torch.argmax(output, dim=1)
    comparisons = torch.eq(output, eoscomp).to(torch.float)

    wandb.log({"final_acc": torch.mean(comparisons)})

    for comparison, params in zip(comparisons, parameters):
        params.insert(0, comparison.item())

    df = pd.DataFrame(parameters)
    df = df.rename(
        columns={
            0: "Correct",
            2: "EOS",
            3: "M1",
            4: "M2",
            5: "SHFT",
            6: "DIST",
            7: "EXRAD",
            8: "RA",
            9: "DEC",
            10: "POL",
            11: "spec",
        }
    )

    df.to_csv("./for_ligo_cluster/c_results.csv")

    wandb.save("./for_ligo_cluster/c_results.csv")
    wandb.save(df)
    wandb.finish(34)
    print(35)
