from ...utilites.noise import (
    generate_colored_noise,
    get_time_domain_strain,
    get_frequency_domain_strain,
    to_pycbc_timeseries,
)
from ..._filepaths._filepaths import ligopsd_path
from ...utilites._preprocessing import pad_width, window
from ...utilites._preprocessing import wt
from ...utilites.fastnoise import wavelet_transform
import numpy as np


def noise_injection(debug=False, passthrough=False, wt = wavelet_transform):

    def inner(inp, size=45 * 400):
        signal = inp["signal"]
        delta_t = inp["pm_time"][-1] - inp["pm_time"][0]
        sam_p = inp["params"]["sam_p"]
        fnoise = generate_colored_noise(
            psd_file=ligopsd_path,
            sampling_frequency=1 / sam_p,
            duration=size * sam_p,
        )
        tds = get_time_domain_strain(fnoise[0], sampling_frequency=1 / sam_p)
        pw = window(tds)
        np.multiply(tds, pw, out=tds)
        tds = wt(tds,sam_p = sam_p)
        if not passthrough:
            inp["signal"] += tds[:, ::45]
        if debug:
            return inp, tds, tds[:, ::45]
        return inp

    return inner
