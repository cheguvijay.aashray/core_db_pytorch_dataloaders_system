import numpy as np
import random as r
import pathlib as p
from ..._filepaths._filepaths import noisecache_path
import functools


class generate_noise:
    def __init__(
        self, noisecache_dir=noisecache_path, size=400, dilation=50, batch_size=64
    ):
        self.file_list = [file for file in noisecache_dir.glob("*.npy")]
        self.size = size
        self.dilation = dilation
        self.file = np.memmap(
            self.file_list[0], mode="r", shape=(400, 40000000), dtype=np.float64  
        )
        self.lenfile = self.file.shape[1]
        self.start = 0
        self.batch_size = batch_size
        self.set_chunk()

    def set_chunk(self):
        self.chunk = self.file[
            :,
            self.start : self.start + (self.size * self.dilation) + self.batch_size - 1,
        ]
        self.chunkpos = 0

    def __call__(self, inp):
        inp["signal"] += self.chunk[
            :,
            self.chunkpos : self.chunkpos + (self.size * self.dilation) : self.dilation,
        ]
        self.chunkpos += 1
        if self.chunkpos == self.batch_size:
            self.start = r.randint(
                0, self.lenfile - (self.size * self.dilation + self.batch_size + 1)
            )
            self.set_chunk()
        return inp
