import torch


def to_tensor(device="cpu"):
    def inner(a, *args):
        for i in a:
            if torch.is_tensor(i):
                i.to(device=device)
            else:
                i = torch.tensor(i).to(device=device)
        return list(a)

    return inner


class to_tensor_clean:
    def __init__(self, device="cpu", debug=False):
        self.device = device
        self.debug = debug

    def __call__(self, a):
        a["params"]["ra"] = a["params"]["angle"][0]
        a["params"]["dec"] = a["params"]["angle"][1]
        a["params"]["psi"] = a["params"]["angle"][2]
        del a["params"]["angle"]
        del a["params"]["sam_p"]
        if self.debug == True:
            print(a["params"])
        a["params"].move_to_end("spec")
        return (
            torch.tensor(a["signal"]).to(self.device),
            torch.tensor([i for i in a["params"].values() if type(i) is not dict]).to(
                self.device
            ),
        )
