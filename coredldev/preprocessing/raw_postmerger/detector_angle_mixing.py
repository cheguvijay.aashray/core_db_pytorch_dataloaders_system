import numpy as np
from pycbc.detector import Detector
from pycbc.types import TimeSeries
from pycbc.waveform import taper_timeseries

tc = 1242443167


class detector_angle_mixing:
    def __init__(self, detector_name="H1"):
        self.detector_name = detector_name

    def __call__(self, inp):
        self.detector = Detector(self.detector_name)
        hplus = TimeSeries(inp["hplus"], delta_t=inp["params"]["sam_p"], epoch=tc)
        hcross = TimeSeries(inp["hcross"], delta_t=inp["params"]["sam_p"], epoch=tc)
        ra, dec, pol = inp["params"]["angle"]
        hplus = taper_timeseries(hplus, "startend")
        hcross = taper_timeseries(hcross, "startend")
        signal = self.detector.project_wave(
            hplus, hcross, ra, dec, pol, method="lal", reference_time=tc
        )
        inp["signal"] = signal.numpy()
        del self.detector
        del hplus
        del hcross
        del signal
        del inp["hplus"]
        del inp["hcross"]
        return inp
