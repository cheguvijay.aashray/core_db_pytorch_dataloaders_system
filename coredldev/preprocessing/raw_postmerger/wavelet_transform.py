from ...utilites._preprocessing import pad_width, window
from ...utilites.fastnoise import wavelet_transform as fwt

# from ...utilites._NoiseGenerator import NoiseGenerator
# from ..._filepaths._filepaths import ligopsd_path
import numpy as np


class wavelet_transform:
    def __init__(self, debug=False, wt=fwt, gpu=False, device=-1, n_jobs=1):
        self.debug = debug
        self.wt = wt
        self.gpu = gpu
        self.device = device
        self.n_jobs = n_jobs

    def __call__(
        self,
        x={},
        signal_name="signal",
        param_name="params",
        shift="percent_shift",
    ):
        data = x[signal_name]
        params = x[param_name]
        sam_p = params["sam_p"]
        if shift == "percent_shift":
            shift = params[shift]
        else:
            pass
        pw = window(data)

        raw_wt = self.wt(
            np.multiply(pw, data),
            sam_p=sam_p,
            device=self.device,
            gpu=self.gpu,
            n_jobs=self.n_jobs,
        )
        # print(raw_wt.shape)
        raw_wt = raw_wt[:, ::50]
        # print(raw_wt.shape)

        x[signal_name] = pad_width(raw_wt, percent_shift=shift)
        if self.debug:
            return x, raw_wt, pw
        return x
