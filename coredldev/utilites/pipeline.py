import time
import copy

class pipeline:
    def __init__(self, pipeline, debug=False):
        self.pipeline = pipeline.values()
        self.names = pipeline.keys()
        self.debug = debug

    def __call__(self, base):
        for key, val in zip(self.names, self.pipeline):
            base = val(base)
        return base


class tpipeline:
    def __init__(self, pipeline, debug=True):
        self.pipeline = pipeline.values()
        self.names = list(pipeline.keys())
        self.debug = debug

    def __call__(self, base: dict):
        a = {}
        a["start"] = copy.deepcopy(base)
        for key, val in zip(self.names, self.pipeline):
            ptime = time.time()
            a[key] = copy.deepcopy(val(base))
            base = a[key]
            print(f"[{key}:{round(time.time() - ptime,5)}s] ", end="")
        print()
        return a[self.names[-1]]
