import torch
import torch.nn as nn
import torch.optim as optim
import torch.distributed as dist
import torch.multiprocessing as mp
from torch.nn.parallel import DistributedDataParallel as DDP
import os
from data import get_dataloader
from vit_pytorch import ViT, deepvit
# Define your machine learning model

def gelublock(nnin, nnout):
    return nn.Sequential( nn.Linear(nnin, nnout) ,nn.GELU())

def get_model():
    # return nn.Sequential(
    #     gelublock(64, 128),
    #     gelublock(128, 256),
    #     gelublock(256, 128),
    #     gelublock(128, 64),
    #     gelublock(64, 10),
    # )
    return deepvit.DeepViT(image_size = 8*4, patch_size = 4, num_classes = 10, dim = 64, depth = 6, heads = 4, mlp_dim = 128, dropout = 0.3, emb_dropout = 0.3,channels = 1)

def train(rank, world_size):
    # Initialize the distributed training environment

    os.environ["MASTER_ADDR"] = "localhost"
    os.environ["MASTER_PORT"] = "29501"
    dist.init_process_group(backend='nccl', rank=rank, world_size=world_size)

    # Create the dataset and data loader
    dataloader = get_dataloader(200)

    # Create the model and move it to the GPU
    model = get_model()
    model = model.to(rank)
    model = DDP(model, device_ids=[rank])

    # Define the loss function and optimizer
    criterion = nn.CrossEntropyLoss()
    optimizer = optim.Adam(model.parameters(), lr=0.001)

    # Training loop
    for epoch in range(400):
        losses = []
        for inputs, labels in dataloader:
            ish = inputs.shape
            inputs = inputs.to(rank)
            labels = labels.to(rank).to(torch.long)

            # Forward pass
            # print("inputs",inputs)
            outputs = model(inputs)
            # print("outputs",outputs)
            # print("labels",labels)
            loss = criterion(outputs, labels)
            # Backward pass and optimization
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            losses.append(loss.item())
            # break
        print(epoch, sum(losses)/len(losses))
        # break
    

    # Clean up the distributed training environment
    dist.destroy_process_group()

if __name__ == '__main__':
    print("started")
    # Set the number of GPUs and processes
    num_gpus = torch.cuda.device_count()
    num_processes = num_gpus

    # Spawn the processes
    print("spawning")
    mp.spawn(train, args=(num_processes,), nprocs=num_processes)
