import torch
from torch.utils.data import Dataset
from torch.utils.data.distributed import DistributedSampler
from sklearn.datasets import load_digits
import torch.nn.functional as F

class SklearnDigitsDataset(Dataset):
    def __init__(self):
        digits = load_digits()
        self.data = torch.tensor(digits.data, dtype=torch.float32)
        self.targets = torch.tensor(digits.target, dtype=torch.long)

    def __len__(self):
        return len(self.data)

    def __getitem__(self, index):
        out = torch.reshape(((self.data[index] + torch.normal(2,1,(64,)))/15),(8,8)).view(1,8,8)
        out = F.interpolate(out,scale_factor = 4)
        return out,self.targets[index]

dataset = SklearnDigitsDataset()

def get_dataloader(batch_size):
    return torch.utils.data.DataLoader(
    dataset,
    batch_size=batch_size,
    sampler=DistributedSampler(dataset)
)
