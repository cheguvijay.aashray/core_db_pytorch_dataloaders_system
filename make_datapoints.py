from coredldev.finders.distance_scaling.h5 import h5Finder
import pickle
import pathlib as p
import numpy as np
import sys

print([float(i) for i in sys.argv[1].split()])

finder = h5Finder(distances = np.array([float(i) for i in sys.argv[1].split()]),shiftpercents=[0])
print(finder,len(finder.datapoints))
pickle.dump(finder.datapoints,open(p.Path(__file__).parent / "datapoints.p","wb"))
