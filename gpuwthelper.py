from coredldev.preprocessing.raw_postmerger.wavelet_transform import wavelet_transform
import numpy as np
import matplotlib.pyplot as plt
import cupy as cp

gpus = list(range(cp.cuda.runtime.getDeviceCount()))
import multiprocessing
from coredldev.utilites.fastnoise import wavelet_transform as wt


class Parallel:
    def __init__(self, n_jobs):
        if n_jobs < 0:
            n_jobs = multiprocessing.cpu_count() + n_jobs + 1
        elif n_jobs > multiprocessing.cpu_count():
            n_jobs = multiprocessing.cpu_count()
        self.n_jobs = n_jobs
        self.pool = multiprocessing.Pool(n_jobs)

    def process(self, job, arg, asarray=False):
        processed = self.pool.map(job, arg)

        if asarray:
            processed = np.array(processed)

        return processed

    def close(self):
        self.pool.close()
        self.pool.terminate()
        self.pool.join()


def wthelper(x):
    return wt(**x)
